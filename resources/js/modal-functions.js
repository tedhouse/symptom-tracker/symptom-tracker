$(document).ready(function () {

    $('#deleteConfirmationModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $(this).find("form").attr("action", button.data("action"));
    });

    $('#confirmationModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $(this).find("form").attr("action", button.data("action"));

        // Check if method was specified
        if (button.data("method") != null && button.data("method") != "") {
            $(this).find("input[name='_method']").val(button.data("method"));
        }
        else {
            $(this).find("input[name='_method']").remove();
        }

    });

});
