var map;
var markers = [];
var markerCluster;
var heatMapData = [];
var heatMapLayer;

symptomSwitch = (value, id) => {
    if (value) {
        markers.forEach(marker => {
            marker.symptoms.forEach(symptom => {
                if (symptom.id == id) {
                    marker.setVisible(true);
                }
            });
        });
    }
    else {
        markers.forEach(marker => {
            marker.symptoms.forEach(symptom => {
                if (symptom.id == id) {
                    marker.setVisible(false);
                }
            });
        });
    }
    refreshMarkerCluster();
}

redMarkerSwitch = (value) => {
    if (value) {
        markers.forEach(marker => {
            if (marker.color == 'red') {
                marker.setVisible(true);
            }
        });
    }
    else {
        markers.forEach(marker => {
            if (marker.color == 'red') {
                marker.setVisible(false);
            }
        });
    }
    refreshMarkerCluster();
}

yellowMarkerSwitch = (value) => {
    if (value) {
        markers.forEach(marker => {
            if (marker.color == 'yellow') {
                marker.setVisible(true);
            }
        });
    }
    else {
        markers.forEach(marker => {
            if (marker.color == 'yellow') {
                marker.setVisible(false);
            }
        });
    }
    refreshMarkerCluster();
}

greenMarkerSwitch = (value) => {
    if (value) {
        markers.forEach(marker => {
            if (marker.color == 'green') {
                marker.setVisible(true);
            }
        });
    }
    else {
        markers.forEach(marker => {
            if (marker.color == 'green') {
                marker.setVisible(false);
            }
        });
    }
    refreshMarkerCluster();
}

heatMapSwitch = (value) => {
    if (value) {
        showHeatMap();
    }
    else {
        hideHeatMap();
    }
}

markerClusterSwitch = (value) => {
    if (value) {
        showMarkerCluster();
    }
    else {
        hideMarkerCluster();
    }
}

showHeatMapView = () => {
    hideMarkers();
    showHeatMap();
}

showMarkerView = () => {
    hideHeatMap();
    showMarkers();
}

showBothViews = () => {
    showMarkers();
    showHeatMap();
}

hideAllViews = () => {
    hideMarkers();
    hideHeatMap();
}

hideMarkers = () => {
    markers.forEach(marker => {
        marker.setVisible(false);
    });
    document.getElementById("redMarkerSwitch").checked = false;
    document.getElementById("yellowMarkerSwitch").checked = false;
    document.getElementById("greenMarkerSwitch").checked = false;
}

showMarkers = () => {
    markers.forEach(marker => {
        marker.setVisible(true);
    });
    document.getElementById("redMarkerSwitch").checked = true;
    document.getElementById("yellowMarkerSwitch").checked = true;
    document.getElementById("greenMarkerSwitch").checked = true;
}

hideHeatMap = () => {
    heatMapLayer.setData([]);
}

showHeatMap = () => {
    heatMapLayer.setData(heatMapData);
}

hideMarkerCluster = () => {
    markerCluster.setMap(null);
}

showMarkerCluster = () => {
    markerCluster.setMap(map);
}

refreshMarkerCluster = () => {
    if (markerCluster.getMap()) {
        var tempMarkers = [];

        markers.forEach(marker => {
            if (marker.visible) {
                tempMarkers.push(marker);
            }
        });

        markerCluster.clearMarkers();
        markerCluster.addMarkers(tempMarkers);
        markerCluster.repaint();
    }
}

// Hide or show markers based on date
refreshDate = () => {
    if (document.getElementById('inputDateFrom').value != "") {
        document.getElementById("inputDateTo").setAttribute("min", document.getElementById('inputDateFrom').value);
    }
    else {
        document.getElementById("inputDateTo").setAttribute("min", "2020-03-26");
    }

    var dateFrom = document.getElementById('inputDateFrom').value;
    var dateTo = document.getElementById('inputDateTo').value;

    markers.forEach(marker => {

        marker.setVisible(true);

        if (dateFrom && dateTo) {
            if (marker.created_at > dateFrom && marker.created_at < dateTo) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }
        else {
            if (dateFrom) {
                if (marker.created_at > dateFrom) {
                    marker.setVisible(true);
                }
                else {
                    marker.setVisible(false);
                }
            }

            if (dateTo) {
                if (marker.created_at < dateTo) {
                    marker.setVisible(true);
                }
                else {
                    marker.setVisible(false);
                }
            }
        }

    });
    refreshMarkerCluster();
}

initMap = () => {

    // fetch reports
    getDataFromAPI('/api/reports/').then(reports => {

        const mapSettings = {
            // view of the philippines
            center: {
                lat: 11,
                lng: 123
            },
            zoom: 5,
            // disable satellite view
            mapTypeControl: false,
            // disable street view
            streetViewControl: false,
            styles: [{
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit",
                "stylers": [{
                    "visibility": "off"
                }]
            }
            ],
        };

        // map
        map = new google.maps.Map(document.getElementById('map'), mapSettings);

        // for each report
        reports.forEach(report => {

            // check if coordinates exist
            if (report.lat != null && report.lng != null) {

                var location = {
                    lat: parseFloat(report.lat),
                    lng: parseFloat(report.lng)
                };

                // default icon is green
                var pinColor = '4caf50';
                var color = 'green';

                // weight determines the color in the heatmap depending on the symptoms
                var weight = 15;

                // change icon to yellow if there's at least one symptom
                if (report.symptoms.length > 0) {
                    pinColor = 'ffee58';
                    color = 'yellow';
                    weight = 50;
                }

                // change icon color to red if one of the symptoms is fever
                report.symptoms.forEach(symptom => {
                    if (symptom.name.indexOf('Fever') >= 0 || symptom.name.indexOf('fever') >= 0) {
                        pinColor = 'b71c1c';
                        color = 'red';
                        weight = 100;
                    }
                });

                var icon = {
                    url: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld= |' + pinColor,
                    size: new google.maps.Size(21, 34),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(4, 19),
                    scaledSize: new google.maps.Size(11, 20) // scaled size
                }

                var marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: icon,
                    color: color,
                    symptoms: report.symptoms,
                    created_at: report.created_at,
                });
                markers.push(marker);

                var heatmapMarker = {
                    location: new google.maps.LatLng(location.lat, location.lng),
                    weight: weight
                };
                heatMapData.push(heatmapMarker);

            }
        });

        var options = {};

        markerCluster = new MarkerClusterer(null, markers, options);
        markerCluster.setStyles(markerCluster.getStyles().map(function (style) {
            style.textColor = '#fff';
            return style;
        }));

        heatMapLayer = new google.maps.visualization.HeatmapLayer({
            data: heatMapData,
            map: map,
            dissipating: false,
            radius: 0.0075 * map.getZoom(),
            maxIntensity: 360,
        });

        hideHeatMap();

    });
}
