
share = () => {
  if (navigator.share) {
    navigator.share({
      title: 'symptom-tracker.tedhosue.org',
      text: 'Answer the survey to help fight COVID-19',
      url: 'https://symptom-tracker.tedhouse.org',
    })
      .then(() => console.log('Successful share'))
      .catch((error) => console.log('Error sharing', error));
  }
  else {
    copyToClipboard();
  }
}

copyToClipboard = () => {
  var dummy = document.createElement('input');
  text = window.location.href;
  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand('copy');
  document.body.removeChild(dummy);
  alert("The link is on the clipboard, you can now share it!");
}
