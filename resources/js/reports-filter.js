$(document).ready(function () {
  let urlParams = new URLSearchParams(window.location.search);
  let minParam = 0;
  let maxParam = 116;

  if (urlParams.has('min_age')) {
    minParam = parseInt(urlParams.get('min_age'));
    maxParam = parseInt(urlParams.get('max_age'));
  }

  $("#slider-range").slider({
    range: true,
    min: 0,
    max: 116,
    values: [minParam, maxParam],
    slide: function (event, ui) {
      $("#minAge").val(ui.values[0] + " yrs old");
      $("#maxAge").val(ui.values[1] + " yrs old");
    }
  });

  $("#minAge").val($("#slider-range").slider("values", 0) + " yrs old");
  $("#maxAge").val($("#slider-range").slider("values", 1) + " yrs old");

  $('#date-range').daterangepicker({
    "showDropdowns": true,
    "showWeekNumbers": true,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "alwaysShowCalendars": true,
    "opens": "left"
  }, function (start, end, label) {
    $("#dateLabel").text("(" + label + ")");
    $("#minDate").val(start.format('YYYY-MM-DD'));
    $("#maxDate").val(end.format('YYYY-MM-DD'));
  });

  function toggleAgeSlider() {
    if ($("#ageSelect").is(":checked")) {
      $("#slider-range").slider("option", "disabled", false);
    } else {
      $("#slider-range").slider("option", "disabled", true);
    }
  }

  toggleAgeSlider();

  $("#ageSelect").change(function () {
    toggleAgeSlider();
  });

  $("#sexSelect").change(function () {
    if ($("#sexSelect").is(":checked")) {
      $("#sexInput").prop('disabled', false);
    } else {
      $("#sexInput").prop('disabled', true);
    }
  });

  $("#symptomNumSelect").change(function () {
    if ($("#symptomNumSelect").is(":checked")) {
      $("#symptomNumInput").prop('disabled', false);
    } else {
      $("#symptomNumInput").prop('disabled', true);
    }
  });
});
