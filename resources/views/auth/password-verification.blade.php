@extends('layouts.auth.index')

@section('content')
  <div class="bg-dark h-100">
    <div class="container h-100">
      <div class="row justify-content-center d-flex" style="height: 80%;">
        <div class="col-md-6 align-self-center">

          <div class="mt-4">
            <a href="{{ route('survey') }}">
              <h1 class="text-white text-center">
                SymptomTracker
              </h1>
            </a>
          </div>

          <card-basic title="Create Password">
            <form method="POST" action="{{ route('password-store') }}">
              @csrf
              <input type="hidden" name="token" value="{{ $token }}" readonly>
              <div class="form-group row">
                <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>
                <div class="col-md-12">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ request()->get('email') ?? old('email') }}" required autocomplete="email" autofocus readonly>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>
                <div class="col-md-12">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                  @error('password')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>
                <div class="col-md-12">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
              </div>
              <div class="form-group row mb-0">
                <div class="col-md-12">
                  <button type="submit" class="btn bg-dark btn-block text-white">
                    {{ __('Save Password') }}
                  </button>
                </div>
              </div>
            </form>
          </card-basic>

        </div>
      </div>
    </div>
  </div>
@endsection
