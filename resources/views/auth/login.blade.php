@extends('layouts.auth.index')

@section('content')
  <div class="bg-dark h-100">
    <div class="container h-100">
      <div class="row justify-content-center d-flex" style="height: 80%;">
        <div class="col-md-6 align-self-center">

          <div class="mt-4">
            <a href="{{ route('survey') }}">
              <h1 class="text-white text-center">
                SymptomTracker
              </h1>
            </a>
          </div>

          <card-basic title="Login" class="mt-4">
            <div>
              <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group row">
                  <div class="col-md-12">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                      value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                      name="password" required autocomplete="current-password" placeholder="Password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>
                      <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                      </label>
                    </div>
                  </div>
                </div>

                <div class="row pt-4 mb-0 justify-content-center">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-dark btn-block btn-raised text-white">
                      {{ __('Login') }}
                    </button>
                  </div>
                </div>

                <div class="row pt-4 mb-0 justify-content-center">
                  <div class="col-12 text-center">
                    @if(Route::has('password.request'))
                    <a class="text-center" href="{{ route('password.request') }}">
                      {{ __('Forgot your password?') }}
                    </a>
                    @endif
                  </div>
                </div>

              </form>
            </div>

          </card-basic>

        </div>
      </div>
    </div>
  </div>
@endsection
