@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
      )))
      @slot('current_page', 'Provinces')
    @endcomponent

    <card-basic title="Add Province" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.provinces.store') }}" method="POST">
        @csrf

        <div class="alert alert-warning" role="alert">
          If the region you want isn't showing up in the choices below, add that region first in the database. Go <a href="{{ route('admin.regions.index') }}">here</a>.
        </div>

        <div class="form-group">
          <label for="inputRegion" class="m-0">Region</label>
          <select class="form-control" id="inputRegion" name="region">
            @foreach($regions as $region)
              <option value="{{ $region->id }}">{{ $region->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="inputName">Province Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required>
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

    @component('components.cards.provinces')
      @slot('provinces', $provinces)
    @endcomponent

  </div>
@endsection
