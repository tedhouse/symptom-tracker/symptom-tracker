@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($province->region->name, route('admin.regions.show', ['region' => $province->region->id])),
      )))
      @slot('current_page', $province->name)
    @endcomponent

    <card-basic title="{{ $province->name }}" class="mt-4">
      <template v-slot:options>
        <options-dropdown>
          <a class="dropdown-item" href="{{ route('admin.provinces.edit', ['province' => $province->id]) }}">
            Edit
          </a>
          <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
            data-action="{{ route('admin.provinces.destroy', ['province' => $province->id]) }}">
            Delete
          </button>
        </options-dropdown>
      </template>
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>Cities</th>
                <td>{{ $province->cities->count() }}</td>
              </tr>
              <tr>
                <th>Region</th>
                <td>
                  <a href="{{ route('admin.regions.show', ['region' => $province->region->id]) }}">
                    {{ $province->region->name }}
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

    @component('components.cards.cities')
      @slot('cities', $province->cities)
    @endcomponent

  </div>
@endsection
