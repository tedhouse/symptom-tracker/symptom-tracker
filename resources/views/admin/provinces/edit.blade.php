@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($province->region->name, route('admin.regions.show', ['region' => $province->region->id])),
        array($province->name, route('admin.provinces.show', ['province' => $province->id])),
      )))
      @slot('current_page', 'Edit')
    @endcomponent

    <card-basic title="Update Province" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.provinces.update', ['province' => $province->id]) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="alert alert-warning" role="alert">
          If the region you want isn't showing up in the choices below, add that region first in the database. Go <a href="{{ route('admin.regions.index') }}">here</a>.
        </div>

        <div class="form-group">
          <label for="inputRegion" class="m-0">Region</label>
          <select class="form-control" id="inputRegion" name="region">
            @foreach($regions as $region)
              @if($region->id == $province->region_id)
                <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
              @else
                <option value="{{ $region->id }}">{{ $region->name }}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="inputName">Province Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required value="{{ $province->name }}">
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
