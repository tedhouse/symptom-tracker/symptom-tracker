@extends('layouts.admin.index')

@section('content')
  <div class="container-fluid">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Reports', route('admin.reports.index')),
      )))
      @slot('current_page', 'Problems')
    @endcomponent

    @component('components.cards.reports')
      @slot('reports', $reports)
    @endcomponent

    @if($reports->hasPages())
    <card-basic class="mt-4">
      <div class="row justify-content-center">
        {{ $reports->appends(request()->input())->links() }}
      </div>
    </card-basic>
    @endif

  </div>
@endsection
