@extends('layouts.admin.index')

@section('content')
  <div class="container-fluid">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Reports')
    @endcomponent

    <card-basic class="mt-4">
      <a href="{{ route('admin.reports.problems') }}" class="btn btn-raised btn-primary">
        View Flagged and Faked Reports
      </a>
    </card-basic>

    @include('components.cards.reports-filter')

    @component('components.cards.reports')
      @slot('reports', $reports)
    @endcomponent

    @if($reports->hasPages())
    <card-basic class="mt-4">
      <div class="row justify-content-center">
        {{ $reports->appends(request()->input())->links() }}
      </div>
    </card-basic>
    @endif

  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/reports-filter.js') }}" defer></script>
@endsection
