@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Profile')
    @endcomponent

    
    {{-- Update Password --}}
    <card-basic title="Update Password" class="mt-4">
        <form method="POST" action="{{ route('admin.profile.password.update') }}">
            @csrf
            <div class="form-group">
                <label for="inputCurrentPassword" class="bmd-label-floating">Current Password</label>
                <input type="password" class="form-control" id="inputCurrentPassword" name="current" required>
            </div>
            <div class="form-group">
                <label for="inputNewPassword" class="bmd-label-floating">New Password</label>
                <input type="password" class="form-control" id="inputNewPassword" name="new" required>
            </div>
            <div class="form-group">
                <label for="inputNewPasswordConfirmation" class="bmd-label-floating">Confirm New Password</label>
                <input type="password" class="form-control" id="inputNewPasswordConfirmation" name="confirm" required>
            </div>
            <button-submit text="Update" icon="edit" class="float-right" />
        </form>
    </card-basic>
  </div>
@endsection
