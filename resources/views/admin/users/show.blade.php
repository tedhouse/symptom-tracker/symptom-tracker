@extends('layouts.admin.index')

@section('content')
  <div class="container">
  
    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Users', route('admin.users.index')),
      )))
      @slot('current_page', $user->name)
    @endcomponent

    <card-basic title="Users" class="mt-4">
        <template v-slot:image>
            <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Joined</td>
                    <td>Last Login</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                    {{ $user->name }}
                    </td>
                    <td>
                    {{ $user->email }}
                    </td>
                    <td>
                    {{ $user->created_at ? $user->created_at->diffForHumans() : '' }}
                    </td>
                    <td>
                    {{ $user->lastLogin ? $user->lastLogin->created_at->diffForHumans() : '' }}
                    </td>
                </tr>
                </tbody>
            </table>
            </tr>
        </template>
    </card-basic>

    <card-basic title="Logins" class="mt-4">
        <template v-slot:image>
            <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>IP Address</td>
                    <td>Date</td>
                </tr>
                </thead>
                <tbody>
                    @foreach($logins as $login)
                        <tr>
                            <td>
                            {{ $login->ip_address}}
                            </td>
                            <td>
                            {{ $login->created_at->diffForHumans() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </tr>
        </template>
    </card-basic>

    
    @if($logins->hasPages())
    <card-basic class="mt-4">
      <div class="row justify-content-center">
        {{ $logins->appends(request()->input())->links() }}
      </div>
    </card-basic>
    @endif
  </div>
@endsection