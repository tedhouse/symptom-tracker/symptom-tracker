@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Users', route('admin.users.index')),
      )))
      @slot('current_page', 'Create')
    @endcomponent
    
    <card-basic title="Add Admin" class="mt-4">
        @include('components.errors')
        <form action="{{ route('admin.users.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="inputName" class="bmd-label-floating">Name</label>
                <input type="text" class="form-control" id="inputName" name="name" required>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="bmd-label-floating">Email</label>
                <input type="email" class="form-control" id="inputEmail" name="email" required>
            </div>
            <button-submit text="Add" class="float-right"/>
        </form>
    </card-basic>
  </div>
@endsection
