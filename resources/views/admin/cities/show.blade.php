@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($city->province->region->name, route('admin.regions.show', ['region' => $city->province->region->id])),
        array('Provinces', route('admin.provinces.index')),
        array($city->province->name, route('admin.provinces.show', ['province' => $city->province->id])),
        array('Cities', route('admin.cities.index')),
      )))
      @slot('current_page', $city->name)
    @endcomponent

    <card-basic title="{{ $city->name }}" class="mt-4">
      <template v-slot:options>
        <options-dropdown>
          <a class="dropdown-item" href="{{ route('admin.cities.edit', ['city' => $city->id]) }}">
            Edit
          </a>
          <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
            data-action="{{ route('admin.cities.destroy', ['city' => $city->id]) }}">
            Delete
          </button>
        </options-dropdown>
      </template>
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>Barangays</th>
                <td>{{ $city->barangays->count() }}</td>
              </tr>
              <tr>
                <th>Province</th>
                <td>
                  <a href="{{ route('admin.provinces.show', ['province' => $city->province->id]) }}">
                    {{ $city->province->name }}
                  </a>
                </td>
              </tr>
              <tr>
                <th>Region</th>
                <td>
                  <a href="{{ route('admin.regions.show', ['region' => $city->province->region->id]) }}">
                    {{ $city->province->region->name }}
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

    @component('components.cards.barangay-create')
      @slot('cities', [$city])
    @endcomponent

    @component('components.cards.barangays')
      @slot('barangays', $city->barangays)
    @endcomponent

    @foreach($city->barangays as $barangay)
      @component('components.cards.barangay-cases-dates')
        @slot('caseStatuses', App\CaseStatus::all())
        @slot('barangay', $barangay)
      @endcomponent
    @endforeach

  </div>
@endsection
