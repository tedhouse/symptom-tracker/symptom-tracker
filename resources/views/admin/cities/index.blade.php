@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array('Provinces', route('admin.provinces.index')),
      )))
      @slot('current_page', 'Cities')
    @endcomponent

    <card-basic title="Add City" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.cities.store') }}" method="POST">
        @csrf

        <div class="alert alert-warning" role="alert">
          If the province you want isn't showing up in the choices below, add that province first in the database. Go <a href="{{ route('admin.provinces.index') }}">here</a>.
        </div>

        <div class="form-group">
          <label for="inputProvince" class="m-0">Province</label>
          <select class="form-control" id="inputProvince" name="province" required>
            @foreach($provinces as $province)
              <option value="{{ $province->id }}">{{ $province->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="inputName">City Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required>
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

    @component('components.cards.cities')
      @slot('cities', $cities)
    @endcomponent

  </div>
@endsection
