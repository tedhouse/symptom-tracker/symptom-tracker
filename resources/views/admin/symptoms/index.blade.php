@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Symptoms')
    @endcomponent

    @component('components.cards.symptoms')
      @slot('symptoms', $symptoms)
    @endcomponent

    <card-basic title="None of the above image" class="mt-4">
      @if(Storage::disk('public')->exists('symptoms/none.jpg'))
        <img style="height: 128px;" src="{{ Storage::disk('public')->url('symptoms/none.jpg') }}">
      @endif
      @include('components.errors')
      <form action="{{ route('admin.symptoms.image.none.update') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <input type="file" name="image" class="form-control-file" id="inputImage" placeholder="Upload image" accept="image/x-png,image/gif,image/jpeg">
        </div>
        <button-submit text="Upload" class="float-right"/>
      </form>
    </card-basic>

    @component('components.cards.symptoms-create')
    @endcomponent

  </div>
@endsection
