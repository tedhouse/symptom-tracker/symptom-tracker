@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Symptoms')
    @endcomponent

    <card-basic title="Edit Symptom" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.symptoms.update', ['symptom' => $symptom->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="inputName">Symptom</label>
          <input type="text" class="form-control" id="inputName" placeholder="Name of symptom" name="name" value="{{ $symptom->name }}">
        </div>
        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
