@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Symptoms', route('admin.symptoms.index')),
      )))
      @slot('current_page', 'Upload Image')
    @endcomponent

    <card-basic title="Upload Image for {{ $symptom->name }}" class="mt-4">
      @if(isset($symptom->image_path))
        <img class="img-thumbnail" style="height: 128px;" src="{{ Storage::disk('public')->url($symptom->image_path) }}">
      @endif
      @include('components.errors')
      <form action="{{ route('admin.symptoms.image.update', ['symptom' => $symptom->id]) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
          <input type="file" name="image" class="form-control-file" id="inputImage" placeholder="Upload image" accept="image/x-png,image/gif,image/jpeg">
        </div>
        <button-submit text="Upload" class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
