@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array($barangay->city->province->region->name, route('admin.regions.show', ['region' => $barangay->city->province->region->id])),
        array($barangay->city->province->name, route('admin.provinces.show', ['province' => $barangay->city->province->id])),
        array($barangay->city->name, route('admin.cities.show', ['city' => $barangay->city->id])),
      )))
      @slot('current_page', $barangay->name)
    @endcomponent

    <card-basic title="{{ $barangay->name }}" class="mt-4">
      <template v-slot:options>
        <options-dropdown>
          <a class="dropdown-item" href="{{ route('admin.barangays.edit', ['barangay' => $barangay->id]) }}">
            Edit
          </a>
          <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
            data-action="{{ route('admin.barangays.destroy', ['barangay' => $barangay->id]) }}">
            Delete
          </button>
        </options-dropdown>
      </template>
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>Latitude</th>
                <td>{{ $barangay->lat }}</td>
              </tr>
              <tr>
                <th>Longitude</th>
                <td>{{ $barangay->lng }}</td>
              </tr>
              <tr>
                <th>City</th>
                <td>
                  <a href="{{ route('admin.cities.show', ['city' => $barangay->city->id]) }}">
                    {{ $barangay->city->name }}
                  </a>
                </td>
              </tr>
              <tr>
                <th>Province</th>
                <td>
                  <a href="{{ route('admin.provinces.show', ['province' => $barangay->city->province->id]) }}">
                    {{ $barangay->city->province->name }}
                  </a>
                </td>
              </tr>
              <tr>
                <th>Region</th>
                <td>
                  <a href="{{ route('admin.regions.show', ['region' => $barangay->city->province->region->id]) }}">
                    {{ $barangay->city->province->region->name }}
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

    <card-basic title="Add Case Summary" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.barangay-cases.store') }}" method="POST">
        @csrf

        <input type="text" name="barangay" value="{{ $barangay->id }}" readonly hidden>

        <div class="row">

          @foreach($caseStatuses as $caseStatus)
            @if(!isset($caseStatus->deactivated_at))
              <div class="col-md-3">
                <div class="form-group">
                  <label for="inputCaseStatus{{ $caseStatus->id }}">{{ $caseStatus->name }}</label>
                  <input type="number" class="form-control" id="inputCaseStatus{{ $caseStatus->id }}" name="case_statuses[{{ $caseStatus->id }}]">
                </div>
              </div>
            @endif
          @endforeach

        </div>

        <div class="form-group">
          <label for="inputDate">Date</label>
          <input type="date" class="form-control" id="inputDate" name="date" value="{{ date('Y-m-d') }}" required>
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

    @component('components.cards.barangay-cases-dates')
      @slot('caseStatuses', $caseStatuses)
      @slot('barangay', $barangay)
    @endcomponent

  </div>
@endsection
