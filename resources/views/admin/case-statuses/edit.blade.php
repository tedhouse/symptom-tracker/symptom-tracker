@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Case Statuses')
    @endcomponent

    <card-basic title="Edit Case Status" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.case-statuses.update', ['case_status' => $caseStatus->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="inputName">Case Status</label>
          <input type="text" class="form-control" id="inputName" name="name" value="{{ $caseStatus->name }}" required>
        </div>
        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
