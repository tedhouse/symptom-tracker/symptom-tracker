@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
      )))
      @slot('current_page', $region->name)
    @endcomponent

    <card-basic title="{{ $region->name }}" class="mt-4">
      <template v-slot:options>
        <options-dropdown>
          <a class="dropdown-item" href="{{ route('admin.regions.edit', ['region' => $region->id]) }}">
            Edit
          </a>
          <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
            data-action="{{ route('admin.regions.destroy', ['region' => $region->id]) }}">
            Delete
          </button>
        </options-dropdown>
      </template>
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>Provinces</th>
                <td>{{ $region->provinces->count() }}</td>
              </tr>
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

    @component('components.cards.provinces')
      @slot('provinces', $region->provinces)
    @endcomponent

  </div>
@endsection
