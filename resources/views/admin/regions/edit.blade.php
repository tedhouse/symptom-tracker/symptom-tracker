@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($region->name, route('admin.regions.show', ['region' => $region->id])),
      )))
      @slot('current_page', 'Edit')
    @endcomponent

    <card-basic title="Add Region" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.regions.update', ['region' => $region->id]) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
          <label for="inputName">Region Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required value="{{ $region->name }}">
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
