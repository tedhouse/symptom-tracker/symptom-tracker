@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Regions')
    @endcomponent

    <card-basic title="Add Region" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.regions.store') }}" method="POST">
        @csrf

        <div class="form-group">
          <label for="inputName">Region Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required>
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

    <card-basic title="Regions" class="mt-4">
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <td>Name</td>
                <td>Created</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              @foreach($regions as $region)
              <tr>
                <td>
                  <a href="{{ route('admin.regions.show', ['region' => $region->id]) }}">
                    {{ $region->name }}
                  </a>
                </td>
                <td>
                  {{ $region->created_at ? $region->created_at->diffForHumans() : '' }}
                </td>
                <td>
                  <options-dropdown icon="more_horiz">
                    <a class="dropdown-item" href="{{ route('admin.regions.edit', ['region' => $region->id]) }}">
                      Edit
                    </a>
                    <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                      data-action="{{ route('admin.regions.destroy', ['region' => $region->id]) }}">
                      Delete
                    </button>
                  </options-dropdown>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

  </div>
@endsection
