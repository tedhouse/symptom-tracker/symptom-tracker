@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')

  <div class="container">

    @if(App::environment('staging'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ __('This is the test site.') }}
        {{ __('If you\'re looking for the real site, go here') }}:
        <a href="https://symptom-tracker.tedhouse.org">
          https://symptom-tracker.tedhouse.org
        </a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif

    <form method="POST" action="{{ route('survey.store') }}">
      @csrf

      <div class="row">

        {{-- Symptoms Survey Column --}}
        <div class="col-md-6">

          <div class="bg-white w-100 p-2 text-center rounded shadow">
            <h4 class="font-gotham-bold my-2 spaced-letters text-aqua text-uppercase">
              {{ __('Symptoms') }}
            </h4>
            <h5 class="text-muted text-lowercase">
              {{ __('Check what applies to you') }}
            </h5>
          </div>

          <card-basic class="mt-4 rounded shadow border-0">

            <div class="row">
              @foreach($symptoms as $symptom)
                <div class="col-6">
                  <div class="d-flex justify-content-center">
                    <div class="form-check my-2">
                      <input class="form-check-input" type="checkbox" id="inputSymptom{{ $symptom->id }}" name="symptoms[]" value="{{ $symptom->id }}" onClick="checkSymptomStatus(); uncheckNoSymptoms(); highlightSymptom(this, 'symptomLabel{{ $symptom->id }}')">
                      <label class="form-check-label" for="inputSymptom{{ $symptom->id }}" name="symptomLabel" id="symptomLabel{{ $symptom->id }}">
                        <div class="d-flex flex-column justify-content-center align-items-center">
                          @if(isset($symptom->image_path))
                            <img class="img-fluid" style="max-height: 116px;" src="{{ Storage::disk('public')->url($symptom->image_path) }}">
                          @endif
                          {{ __($symptom->name) }}
                        </div>
                      </label>
                    </div>
                  </div>
                </div>
              @endforeach

              <div class="col-6">
                <div class="d-flex justify-content-center">
                  <div class="form-check my-2">
                    <input class="form-check-input" type="checkbox" value="true" id="inputNone" name="symptom_none" onClick="uncheckSymptoms(); highlightSymptom(this, 'symptomLabelNone'); checkSymptomStatus()">
                    <label class="form-check-label" for="inputNone" id="symptomLabelNone">
                      <div class="d-flex flex-column justify-content-center align-items-center">
                        @if(Storage::disk('public')->exists('symptoms/none.jpg'))
                          <img style="height: 116px;" src="{{ Storage::disk('public')->url('symptoms/none.jpg') }}">
                        @endif
                        {{ __('None of the above') }}
                      </div>
                    </label>
                  </div>
                </div>
              </div>

            </div>

            <input type="text" name="lat" class="form-control" id="inputLatitude" readonly hidden>
            <input type="text" name="lng" class="form-control" id="inputLongitude" readonly hidden>

          </card-basic>

        </div>

        <div class="col-md-6 mt-md-0 mt-4">

          <div class="bg-white w-100 p-2 text-center rounded shadow">
            <h4 class="font-gotham-bold my-2 spaced-letters text-aqua text-uppercase">{{ __('Information') }}</h4>
            <h5 class="text-muted text-lowercase">{{ __('These are optional') }}</h5>
          </div>

          <card-basic class="mt-4 rounded shadow border-0">

            <div class="d-flex align-items-center col-sm-10 col-md-6">
              <label for="inputAge" class="m-0">{{ __('Age') }}</label>
              <div class="ml-2 w-100">
                <input type="number" class="form-control" id="inputAge" name="age" min="0" max="116">
              </div>
            </div>

            <div class="d-flex align-items-center col-sm-10 col-md-6 mt-4 ">
              <label for="inputSex" class="m-0">{{ __('Gender') }}</label>
              <div class="ml-2 w-100">
                <select class="form-control" id="inputSex" name="sex">
                  <option></option>
                  <option value="1">{{ __('Male') }}</option>
                  <option value="2">{{ __('Female') }}</option>
                  {{-- <option value="9">Other</option> --}}
                  {{-- <option>Rather not say</option> --}}
                </select>
              </div>
            </div>

            <div class="form-check mt-4">
              <input class="form-check-input" value="true" type="checkbox" id="inputTravelledAbroad" name="traveled_abroad">
              <label class="form-check-label" for="inputTravelledAbroad">
                {{ __('Travelled abroad within the last 14 days') }}
              </label>
            </div>

            <div class="form-check mt-4">
              <input class="form-check-input" value="true" type="checkbox" id="inputCloseContact" name="close_contact">
              <label class="form-check-label" for="inputCloseContact">
                {{ __('Close contact with a known COVID-19 patient within the last 14 days') }}
              </label>
            </div>

          </card-basic>

          <div class="mt-4">
            <div class="bg-white w-100 p-2 text-center rounded shadow">
              <h4 class="font-gotham-bold my-2 spaced-letters text-aqua text-uppercase">
                {{ __('Location') }}
              </h4>
              <h5 class="text-muted text-lowercase">
                {{ __('We\'ll randomize this location by a few hundred meters to protect your identity') }}
              </h5>
            </div>

            <div id="gps" class="alert alert-success mt-4 rounded shadow border-0" role="alert">
              {{ __('Please turn on your GPS to generate map.') }}
              {{ __('We do not store your exact location when you submit this survey.') }}
              {{ __('We randomize your location within 1 KM from the original point to protect your identity') }}
            </div>

            <card-basic class="mt-4 rounded shadow border-0">
              <template v-slot:image>
                <div id="map" class="w-100 rounded" style="height: 200px;"></div>
              </template>
            </card-basic>
          </div>

          <div class="d-flex justify-content-center w-100 my-4 rounded shadow border-0">
            <button-submit text="{{ __('Submit') }}" id="submit_btn" class="btn-secondary w-100 py-3 rounded font-gotham-bold text-uppercase cursor-default" disabled/>
          </div>

        </div>

      </div>

      <card-basic class="rounded shadow border-0 mt-4">
        <div class="alert alert-info" role="alert">
          {{ __('Hi!') }}
          {{ __('Students, researchers, and volunteers from De La Salle University - Laguna Campus developed a simple crowdsourcing tool to map COVID-19 symptoms.') }}
          {{ __('This will allow us and health professionals to see possible patterns which would otherwise be undetectable because of the quarantine.') }}
          {{ __('The map pins are randomized to a degree that will allow us to protect your privacy while still being able to generate a community map.') }}
          {{ __('It will only take you 10 seconds to provide your data.') }}
          {{ __('Please submit one entry for each person in your household.') }}
          {{ __('You are encouraged to submit your data again once your set of symptoms change.') }}
          {{ __('Thank you!') }}
        </div>

        <div class="d-flex justify-content-center w-100">
          <a href="{{ route('map') }}">
            {{ __('Check out the map') }}
          </a>
        </div>
      </card-basic>

      <card-basic class="mt-4 text-center rounded shadow border-0">
        {{ __('Share this to your friends and family!') }}
        <div class="d-flex justify-content-center w-100 mt-4">
          <button onclick="share()" type="button" class="btn btn-green w-100 py-3 rounded font-gotham-bold text-uppercase text-center">
            {{ __('Share') }}
          </button>
        </div>
      </card-basic>

    </form>
  </div>
@endsection

@section('scripts')
  <script>
    checkSymptomStatus = () => {
      var sympCheckboxes = document.getElementsByTagName('input');
      var disableBtn = document.getElementById('inputNone');

      // disable submit button if no checkboxes are checked
      var hasChecked = false;
      for (var i = 0; i < sympCheckboxes.length; i++) {
        if (sympCheckboxes[i].checked == true) {
          hasChecked = true;
          break;
        }
      }
      hasChecked ? disableSubmitBtn(false) : disableSubmitBtn(true);
    }

    uncheckSymptoms = () => {
      var symptomCboxes = document.getElementsByName('symptoms[]');
      var symptomLabels = document.getElementsByName('symptomLabel');

      for (var i = 0; i < symptomCboxes.length; i++) {
        symptomCboxes[i].checked = false;
        symptomLabels[i].classList.remove('font-weight-bold');
        symptomLabels[i].classList.remove('text-green');
      }
    }

    uncheckNoSymptoms = () => {
      var noSymp = document.getElementById('inputNone');
      var noSympLabel = document.getElementById('symptomLabelNone');
      noSymp.checked = false;
      noSympLabel.classList.remove('font-weight-bold');
      noSympLabel.classList.remove('text-green');
    }

    disableSubmitBtn = (disabled) => {
      var btn = document.getElementById('submit_btn');

      if (disabled) {
        btn.disabled = true;
        // change button style
        btn.classList.remove('btn-green');
        btn.classList.add('btn-secondary');
        // change cursor style
        btn.classList.remove('cursor-pointer');
        btn.classList.add('cursor-default');
      } else {
        btn.disabled = false;
        // change button style
        btn.classList.remove('btn-secondary');
        btn.classList.add('btn-green');
        // change cursor style
        btn.classList.remove('cursor-default');
        btn.classList.add('cursor-pointer');
      }
    }

    highlightSymptom = (cbox, symptomLabelID) => {
      var symptomLabel = document.getElementById(symptomLabelID);

      if (cbox.checked) {
        symptomLabel.classList.add('font-weight-bold');
        symptomLabel.classList.add('text-green');
      } else {
        symptomLabel.classList.remove('font-weight-bold');
        symptomLabel.classList.remove('text-green');
      }
    }
  </script>
  <script defer>
    var map, infoWindow;

    function doesMapExist() {
      var exists = document.getElementById("map");
      if(exists) {
        initMap();
      }
      else {
        // TODO: sometimes the google maps api will call initMap() when 'map' hasn't loaded yet and it returns an error
        setTimeout(function(){
          doesMapExist();
        }, 1000);
      }
    }

    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        mapTypeControl: false,
        streetViewControl: false,
        zoom: 19
      });
      infoWindow = new google.maps.InfoWindow;

      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          if (pos != null) {
            var element = document.getElementById("gps");
            element.classList.add("d-none");
          }

          var marker = new google.maps.Marker({position: pos, map: map});
          infoWindow.open(map);
          map.setCenter(pos);
          marker.setAnimation(google.maps.Animation.DROP);

          document.getElementById('inputLatitude').value = marker.getPosition().lat();
          document.getElementById('inputLongitude').value = marker.getPosition().lng();

        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
      infoWindow.open(map);
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{ config('settings.google_maps_api') }}&callback=doesMapExist"
  async defer></script>
@endsection
