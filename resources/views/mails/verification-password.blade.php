@component('mail::message')
  # Hello {{ $name }},
  You're choosen to be a user for SymptomTracker PH

  Please click the button below to verify your email address
  @component('mail::button', ['url' => $link])
    Verify Email Address
  @endcomponent

  Sincerely,
  {{ config('app.name', 'SymptomTracker') }}
@endcomponent
