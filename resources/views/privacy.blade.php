@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="bg-white w-100 p-2 text-center rounded shadow">
          {{-- Desktop --}}
          <div class="d-none d-md-block">
            <h3 class="font-gotham-bold my-2 spaced-letters text-aqua">PRIVACY POLICY</h3>
          </div>
          {{-- Mobile --}}
          <div class="d-md-none">
            <h3 class="font-gotham-bold my-2 text-aqua">PRIVACY POLICY</h3>
          </div>
        </div>

        <card-basic class="mt-4 p-1 p-md-4 font-gotham-light">
          <h6 class="font-gotham-light text-muted"><i>Last Updated: April 14, 2020</i></h6>

          <div class="jumbotron bg-light mt-4 py-4">
            <h3 class="font-gotham-bold text-aqua">Contents</h3>

            {{-- Table of contents --}}
            <ul class="list-group list-group-flush">
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#introduction" class="font-gotham text-green">
                  Introduction
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#what-we-collect" class="font-gotham text-green">
                  What User Data We Collect
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#why-we-collect" class="font-gotham text-green">
                  Why We Collect Your Data
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#data-security" class="font-gotham text-green">
                  Safeguarding and Securing the Data
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#cookies" class="font-gotham text-green">
                  Our Cookie Policy
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#other-websites" class="font-gotham text-green">
                  Links to Other Websites
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#restrict-data-collection" class="font-gotham text-green">
                  Restricting the Collection of Your Personal Data
                </link-scroll>
              </li>
            </ul>
          </div>

          {{-- Policies section --}}
          @component('components.privacy-policy')
            @slot('id', 'introduction')
            @slot('title', 'Introduction')
            @slot('content')
              <p>
                This privacy policy ("<code>policy</code>") will help you understand how <em><b>SymptomTracker PH</b></em> ("<code>us</code>", "<code>we</code>", "<code>our</code>") uses and protects the data you provide to us when you visit and use <a class="font-italic" href="{{ url('/') }}" target="blank">{{ url('/') }}</a> ("<code>website</code>", "<code>service</code>").
              </p>
              <p>
                We reserve the right to change this policy at any given time, of which you will be promptly updated. If you want to make sure that you are up to date with the latest changes, we advise that you frequently visit <a href="{{ url('/privacy') }}">this page</a>.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'what-we-collect')
            @slot('title', 'What User Data We Collect')
            @slot('content')
              <p>When you visit the website, we may collect the following data:</p>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  Your IP address
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  Data profile regarding your online behavior on our website
                </li>
              </ul>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'why-we-collect')
            @slot('title', 'Why We Collect Your Data')
            @slot('content')
              <p>We are collecting your data for several reasons:</p>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  To better understand your needs
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  To improve our services and products
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  To customize our website according to your online behavior and personal preferences
                </li>
              </ul>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'data-security')
            @slot('title', 'Safeguarding and Securing the Data')
            @slot('content')
              <p>
                <em><b>SymptomTracker PH</b></em> is committed to securing your data and keeping it confidential. <em><b>SymptomTracker PH</b></em> has done all in its power to prevent data theft, unauthorized access, and disclosure by implementing the latest technologies and software, which help us safeguard all of the information we collect online.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'cookies')
            @slot('title', 'Our Cookie Policy')
            @slot('content')
              <p>
                Once you agree to allow our website to use cookies, you also agree to use the data it collects regarding your online behavior (analyze web traffic, web pages you spend the most time on, and websites you visit).
              </p>
              <p>
                The data we collect by using cookies is used to customize our website to your needs. After we use the data for statistical analysis, the data is completely removed from our systems.
              </p>
              <p>
                Please note that cookies don't allow us to gain control of your computer in any way. They are strictly used to monitor which pages you find useful and which you do not so that we can provide a better experience for you.
              </p>
              <p>
                If you want to disable cookies, you can do it by accessing the settings of your internet browser.
              </p>

              <h4 class="font-gotham my-4">Disable Cookies in Chrome</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  1. Click "<code>Settings</code>" in the top-right corner, marked by the three dots.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  2. Click "<code>Show advanced settings</code>" at the bottom of the screen.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  3. Move on to the "<code>Privacy</code>" section and click on the "<code>Content settings...</code>" button.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  4. The settings for cookies may be changed in the cookies section right at the top.
                </li>
              </ul>

              <h4 class="font-gotham my-4">Disable Cookies in Firefox</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  1. Click on the menu button, marked by the three horizontal lines in the top-right corner and select "<code>Options</code>".
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  2. Move on to the "<code>Privacy</code>" tab on the left side.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  3. Under the "<code>History</code>" option select "<code>Use custom settings for history</code>" in the drop-down box.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  4. Set "<code>Accept third-party cookies</code>" to "<code>Never</code>" in the drop-down box.
                </li>
              </ul>

              <h4 class="font-gotham my-4">Disable Cookies in Microsoft Edge</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  1. Click on the menu button marked by the three dots in the top-right corner and select "<code>Settings</code>".
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  2. Scroll down the bottom of the side panel and click on the "<code>View advanced settings</code>" button.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  3. To disable cookies, select "<code>Block all cookies</code>" in the drop-down box right below the "<code>Cookies</code>" section.
                </li>
              </ul>

              <h4 class="font-gotham my-4">Disable Cookies in Internet Explorer</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  1. Click on the "<code>Settings</code>" icon in the top-right corner and select "<code>Internet options</code>".
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  2. Switch to the "<code>Privacy</code>" tab.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  3. Move the slider all the way up and you should see "<code>Block all cookies</code>" displayed right next to it. To allow cookies you will need to move the slider all the way down instead.
                </li>
              </ul>

              <h4 class="font-gotham my-4">Disable Cookies in Safari</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  1. Click on the "<code>Safari</code>" menu item, and then the "<code>Preference</code>" option from the drop-down menu.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  2. Switch to the "<code>Privacy</code>" tab.
                </li>
                <li class="list-group-item bg-black border-left border-top-0 border-bottom-0 border-green" style="border-left-width: medium !important;">
                  3. Next to "<code>Cookies and website data</code>" select "<code>Always block</code>".
                </li>
              </ul>

              <p class="mt-4">If you would like to know more about how cookies are stored on your browser, <a href="https://www.websitepolicies.com/blog/how-to-disable-clear-cookies" target="blank">visit this page</a>.</p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'other-websites')
            @slot('title', 'Links to Other Websites')
            @slot('content')
              <p>
                Our website contains links that lead to other websites. If you click on these links, <em><b>SymptomTracker PH</b></em> is not held responsible for your data and privacy protection. Visiting those websites is not governed by this privacy policy agreement. Make sure to read the privacy policy documentation of the website(s) you go to from our website.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'restrict-data-collection')
            @slot('title', 'Restricting the Collection of Your Personal Data')
            @slot('content')
              <p>
                At some point, you might wish to restrict the use and collection of your personal data. You can achieve this by emailing <code>privacy@imagineware.ph</code>.
              </p>
              <p>
                <em><b>SymptomTracker PH</b></em> will not lease, sell, or distribute your personal information to any third parties, unless we have your permission. We might do so if the law forces us.
              </p>
            @endslot
          @endcomponent
        </card-basic>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
