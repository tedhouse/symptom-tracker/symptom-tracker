<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

  @include('components.google-analytics')
  @include('components.meta')
  @include('components.icons')

  {{-- Title --}}
  <title>{{ config('app.name', 'Survey Tracker') }}</title>

  {{-- CSS --}}

  {{-- App CSS --}}
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  {{-- Material Icons --}}
  {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons" rel="stylesheet"> --}}

  @yield('styles')

</head>

<body>

  <div id="app">

    <div class="row">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark shadow bg-aqua w-100">
          <div class="container p-2 font-gotham">

            <a class="navbar-brand" href="{{ route('survey') }}">
              <img class="img-responsive m-auto" src="{{ asset('images/logos/logo_white.png') }}" style="height: 32px;" alt="SymptomTracker">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContentTransparentM" aria-controls="navbarSupportedContentTransparentM" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContentTransparentM">
              <ul class="navbar-nav ml-0 ml-md-auto">
                <li class="nav-item ml-0 ml-md-3">
                  <a class="nav-link text-white" href="{{ route('map') }}">
                    {{ __('Map') }}
                  </a>
                </li>
                <li class="nav-item ml-0 ml-md-3">
                  <a class="nav-link text-white" href="{{ route('faq') }}">
                    {{ __('FAQs') }}
                  </a>
                </li>
                <li class="nav-item ml-0 ml-md-3">
                  <a class="nav-link text-white" href="{{ route('about-us') }}">
                    {{ __('About Us') }}
                  </a>
                </li>
              </ul>
            </div>

          </div>
        </nav>
      </div>
      </div>

    {{-- Header --}}
    <div class="container-fluid">
      <div class="row flex-column bg-gradient text-white text-center font-gotham-bold py-4">
        <a href="{{ route('survey') }}" class="text-decoration-none">
          <img src="{{ asset('images/dlsu_64.png') }}" id="banner-img-desktop" alt="De La Salle University logo" style="height: 64px; width: 64px;">
          <img class="img-fluid" src="{{ asset('images/logos/logo_banner_270.png') }}" id="banner-img-desktop" alt="SymptomTracker logo">
        </a>
        <span>together with</span>
        <div class="d-flex justify-content-center my-2">
          <div class="mx-2 d-flex flex-column justify-content-center align-items-center">
            <img src="{{ asset('images/biñan_64.png') }}" id="banner-img-desktop" alt="SymptomTracker logo" style="height: 64px; width: 64px;">
            <span>City Government of Biñan</span>
          </div>
        </div>
      </div>
    </div>

    @yield('content')

    @include('layouts.app.footer')

  </div>

  {{-- JavaScript --}}
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="{{ asset('js/vue.js') }}" defer></script>
  <script src="{{ asset('js/share.js') }}" defer></script>
  {{-- <script src="{{ asset('js/symptom-checkbox-functions.js') }}" defer></script> --}}

  @yield('scripts')

</body>

</html>
