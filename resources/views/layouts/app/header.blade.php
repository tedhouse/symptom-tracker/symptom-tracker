@include('layouts.app.navbar')
<div class="container-fluid position-fixed scroll-transition" id="app-header">
  @include('layouts.app.navbar-transparent')
  {{-- Mobile --}}
  <div class="d-md-none">
    <div class="row bg-gradient text-white text-center font-gotham-bold py-3" style="height: 270px">
      <a href="{{ route('survey') }}" class="m-auto">
        <img src="{{ asset('images/dlsu_64.png') }}" id="banner-img-desktop" alt="De La Salle University logo" style="height: 64px; width: 64px;">
        <img class="img-fluid" src="{{ asset('images/logos/logo_banner_270.png') }}" id="banner-img-mobile" alt="SymptomTracker logo">
      </a>
    </div>
  </div>
  {{-- Desktop --}}
  <div class="d-none d-md-block">
    <div class="row bg-gradient text-white text-center font-gotham-bold py-3" style="height: 270px">
      <a href="{{ route('survey') }}" class="m-auto">
        <img src="{{ asset('images/dlsu_64.png') }}" id="banner-img-desktop" alt="De La Salle University logo" style="height: 64px; width: 64px;">
        <img class="img-fluid" src="{{ asset('images/logos/logo_banner_270.png') }}" id="banner-img-desktop" alt="SymptomTracker logo">
      </a>
    </div>
  </div>
  @include('components.alerts')
</div>
