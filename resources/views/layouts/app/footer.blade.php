<footer class="footer position-relative pt-2 pb-3 bg-light mt-4">
  <div class="container text-muted font-gotham-light">

    <div class="d-flex justify-content-center my-2 flex-wrap">

      <a href="/lang/en" class="text-muted px-2">English</a>
      <a href="/lang/fil" class="text-muted px-2">Filipino</a>
      <a href="/lang/ceb" class="text-muted px-2">Cebuano/Bisaya</a>
      <a href="/lang/hil" class="text-muted px-2">Hiligaynon</a>
      <a href="/lang/bik" class="text-muted px-2">Bikol</a>
      <a href="/lang/ilo" class="text-muted px-2">Ilocano</a>
      <a href="/lang/war" class="text-muted px-2">Waray</a>
      <a href="/lang/cbk" class="text-muted px-2">Chavacano</a>
      <a href="/lang/ko" class="text-muted px-2">한국어</a>

    </div>

    <div class="d-flex justify-content-center my-2">
      &copy; {{ now()->year }} SymptomTrackerPH
    </div>

    <div class="d-flex justify-content-center my-2">

      <a class="px-2" href="https://www.facebook.com/SymptomTracker/" target="blank">
        <img class="image-fluid" src="{{ asset('images/social_media_icons/facebook.png') }}" alt="Facebook" style="height: 16px"/>
      </a>

      <a class="px-2" href="https://twitter.com/Symptom_Tracker" target="blank">
        <img class="image-fluid" src="{{ asset('images/social_media_icons/twitter.png') }}" alt="Facebook" style="height: 16px"/>
      </a>

      <a class="px-2" href="https://www.instagram.com/SymptomTrackerPH/" target="blank">
        <img class="image-fluid" src="{{ asset('images/social_media_icons/instagram.png') }}" alt="Facebook" style="height: 16px"/>
      </a>

    </div>

    <div class="d-flex justify-content-center my-2">

      <a href="{{ url('/privacy') }}" class="text-muted px-2">Privacy Policy</a>

      <a href="{{ url('/faq') }}" class="text-muted px-2">FAQs</a>

      <a href="{{ url('/about-us') }}" class="text-muted px-2">About Us</a>

    </div>

  </div>
</footer>
