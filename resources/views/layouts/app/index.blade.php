<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

  @include('components.google-analytics')
  @include('components.meta')
  @include('components.icons')

  {{-- Title --}}
  <title>{{ config('app.name', 'Survey Tracker') }}</title>

  {{-- CSS --}}

  {{-- App CSS --}}
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  {{-- Material Icons --}}
  {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons" rel="stylesheet"> --}}

  @yield('styles')

</head>

<body>

  <div id="app">
    @include('layouts.app.header')
    <div class="position-absolute w-100 py-0" id="content-wrapper">
      @yield('content')
      @include('layouts.app.footer')
    </div>
  </div>

  {{-- JavaScript --}}
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="{{ asset('js/vue.js') }}" defer></script>
  <script src="{{ asset('js/share.js') }}" defer></script>
  <script src="{{ asset('js/symptom-checkbox-functions.js') }}" defer></script>

  @yield('scripts')

</body>

</html>
