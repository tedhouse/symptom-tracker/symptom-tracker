<div class="row">
  {{-- Mobile --}}
  <nav class="navbar navbar-expand-lg navbar-dark shadow bg-aqua w-100 position-fixed scroll-transition p-0" id="app-navbar-transparent-m" style="z-index: 999; top: 0;">
    <div class="container p-2 font-gotham">

      <a class="navbar-brand" href="{{ route('survey') }}">
        <img class="img-responsive m-auto" src="{{ asset('images/logos/logo_white.png') }}" style="height: 32px;" alt="SymptomTracker">
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContentTransparentM" aria-controls="navbarSupportedContentTransparentM" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContentTransparentM">
        <ul class="navbar-nav ml-0 ml-md-auto">
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('map') }}">
              {{ __('Map') }}
            </a>
          </li>
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('faq') }}">
              {{ __('FAQs') }}
            </a>
          </li>
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('about-us') }}">
              {{ __('About Us') }}
            </a>
          </li>
        </ul>
      </div>

    </div>
  </nav>
  {{-- TODO: Remove this desktop view --}}
  {{-- Desktop --}}
  <nav class="navbar navbar-expand-lg navbar-dark shadow bg-none d-none w-100 position-fixed scroll-transition p-0" id="app-navbar-transparent" style="z-index: 999; top: 0;">
    <div class="container p-2 font-gotham">

      <a class="navbar-brand" href="{{ route('survey') }}">
        <img class="img-responsive m-auto" src="{{ asset('images/logos/logo_white.png') }}" style="height: 32px;" alt="SymptomTracker">
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContentTransparent" aria-controls="navbarSupportedContentTransparent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContentTransparent">
        <ul class="navbar-nav ml-0 ml-md-auto">
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('map') }}">
              {{ __('Map') }}
            </a>
          </li>
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('faq') }}">
              {{ __('FAQs') }}
            </a>
          </li>
          <li class="nav-item ml-0 ml-md-3">
            <a class="nav-link text-white" href="{{ route('about-us') }}">
              {{ __('About Us') }}
            </a>
          </li>
        </ul>
      </div>

    </div>
  </nav>
</div>
