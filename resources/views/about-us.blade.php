@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col">

        <div class="bg-white w-100 p-2 text-center rounded shadow">
          {{-- Desktop --}}
          <div class="d-none d-md-block">
            <h3 class="font-gotham-bold my-2 spaced-letters text-aqua">ABOUT US</h3>
          </div>
          {{-- Mobile --}}
          <div class="d-md-none">
            <h3 class="font-gotham-bold my-2 text-aqua">ABOUT US</h3>
          </div>
        </div>

        <card-basic class="mt-4 p-1 p-md-4 rounded shadow border-0">

          <p class="text-justify">
            The project team is led by Dr. Arnulfo Azcarraga, Mr. Jay Calleja, and Mr. Cyrus Vatandoost Kakhki.  You may reach us at covid.symtracker@gmail.com.  In this extraordinary time, the project is an attempt to provide the public a crowdsourcing platform where they can share their health status and see from a particular vantage point the state of health of their community amidst the threat of COVID-19.
          </p>

          <p class="text-justify">
            The sole purpose of the submitted information is to generate a crowdsourced map accessible to the public.
          </p>

          <p class="text-justify">
            The map may be of use to health care professionals in the fields of public health and epidemiology.  Households and community leaders may also refer to the map as a possible source of important information in this time of enhanced community quarantine in various parts of the country.
          </p>

          <p class="text-justify">
            This is not a research study, nor will the data be used for research. We are collecting symptom data using the crowdsourcing approach because we have the skills and the technology to do it, and we think that the information will help our authorities in leading us through this pandemic. This is just our little contribution to the much bigger effort mounted by our officials and the other leaders of the country.
          </p>

        </card-basic>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
