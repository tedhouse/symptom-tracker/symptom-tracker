<div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-2">
      <div class="modal-header py-4 row justify-content-center">
        <h5 class="modal-title">
          Are you sure you want to delete this?
        </h5>
      </div>
      <div class="modal-footer">
        <form method="POST">
          @method('DELETE')
          @csrf
          <button type="button" class="btn btn-link text-decoration-none" data-dismiss="modal">Cancel</button>
          <button-submit text="Delete" class="btn-danger"/>
        </form>
      </div>
    </div>
  </div>
</div>
