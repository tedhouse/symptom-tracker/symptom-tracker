<div class="mt-5" id="{{ $id }}">
  <h4 class="font-gotham-bold text-aqua mb-3">{{ $title }}</h4>
  <div class="font-gotham text-dark">
    {{ $content }}
  </div>
</div>
