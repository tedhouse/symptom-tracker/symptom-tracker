{{-- CSRF Token --}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- Meta data tags --}}
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=5">
<meta name="description" content="Help us track COVID-19 symptoms on a daily basis">
<meta name="author" content="Survey Tracker">

{{-- Primary Meta Tags --}}
<meta name="title" content="{{ config('app.name', 'SymptomTracker') }}">
<meta name="description" content="Help us track COVID-19 symptoms on a daily basis">
<meta name="keywords" content="symptom, tracker, tedhouse, te3dhouse, dlsu, covid-19">
<meta name="robots" content="index, follow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="English">
<meta name="revisit-after" content="7 days">

{{-- Google / Search Engine Tags --}}
<meta itemprop="name" content="SymptomTracker">
<meta itemprop="description" content="Help us track COVID-19 symptoms on a daily basis">
<meta itemprop="image" content="{{ url('images/og.jpg') }}">

{{-- Open Graph / Facebook --}}
<meta property="og:type" content="website">
<meta property="og:title" content="{{ config('app.name', 'SymptomTracker') }}">
<meta property="og:description" content="Help us track COVID-19 symptoms on a daily basis">
<meta property="og:image" content="{{ url('images/og.jpg') }}">

{{-- Twitter Meta Tags --}}
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="SymptomTracker">
<meta name="twitter:description" content="Help us track COVID-19 symptoms on a daily basis">
<meta name="twitter:image" content="{{ url('images/og.jpg') }}">
