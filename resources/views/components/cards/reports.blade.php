<card-basic title="Reports" href="{{ route('admin.reports.index') }}" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="text-nowrap">Created</th>
            <th class="text-nowrap">Status</th>
            <th class="text-nowrap">Location</th>
            <th class="text-nowrap">Age</th>
            <th class="text-nowrap">Sex</th>
            <th class="text-nowrap">Traveled</th>
            <th class="text-nowrap">Contact</th>
            <th class="text-nowrap">Symptoms</th>
            <th class="text-nowrap">IP Address</th>
            <th class="text-nowrap">Session ID</th>
          </tr>
        </thead>
        <tbody>
          @foreach($reports as $report)
          <tr>
            <td class="text-nowrap" data-toggle="tooltip" data-placement="top" title="{{ $report->created_at }}">
              {{ $report->created_at->diffForHumans() }}
            </td>
            <td class="text-nowrap">

              @if(isset($report->flagged_at))
                <span class="badge badge-warning"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="{{ $report->flagged_at->diffForHumans() }} - {{ $report->flagged_at }}">
                  Flagged
                </span>
              @endif

              @if(isset($report->faked_at))
                <span class="badge badge-danger"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="{{ $report->faked_at->diffForHumans() }} - {{ $report->faked_at }}">
                  Faked
                </span>
              @endif

            </td>
            <td class="text-nowrap">
              <a href="https://www.google.com/maps/{{ '@' }}{{ $report->lat }},{{ $report->lng }},18z" target="_blank">
                {{ $report->lat }}, {{ $report->lng }}
              </a>
            </td>
            <td>
              {{ $report->age }}
            </td>
            <td>
              @if($report->sex == 1)
                Male
              @elseif($report->sex == 2)
                Female
              @endif
            </td>
            <td>
              @if($report->traveled_abroad == 1)
                Yes
              @endif
            </td>
            <td>
              @if($report->close_contact == 1)
                Yes
              @endif
            </td>
            <td class="">
              @foreach($report->symptoms as $symptom)
                <span class="badge badge-secondary">{{ $symptom->name }}</span>
              @endforeach
            </td>
            <td class="text-nowrap">
              <code>{{ $report->ip_address }}</code>
            </td>
            <td>
              <code>{{ $report->session_id }}</code>
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                @if($report->flagged_at == null)
                  <button class="dropdown-item" data-toggle="modal" data-target="#confirmationModal"
                    data-action="{{ route('admin.reports.flag', ['report' => $report->id]) }}">
                    Flag as possible fake
                  </button>
                @else
                  <button class="dropdown-item text-warning" data-toggle="modal" data-target="#confirmationModal"
                    data-action="{{ route('admin.reports.unflag', ['report' => $report->id]) }}">
                    Remove flag
                  </button>
                @endif
                @if($report->faked_at == null)
                  <button class="dropdown-item" data-toggle="modal" data-target="#confirmationModal"
                    data-action="{{ route('admin.reports.fake', ['report' => $report->id]) }}">
                    Flag as fake
                  </button>
                @else
                  <button class="dropdown-item text-warning" data-toggle="modal" data-target="#confirmationModal"
                    data-action="{{ route('admin.reports.unfake', ['report' => $report->id]) }}">
                    Declare not fake
                  </button>
                @endif
                <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.reports.destroy', ['report' => $report->id]) }}">
                  Delete
                </button>
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </template>
</card-basic>
