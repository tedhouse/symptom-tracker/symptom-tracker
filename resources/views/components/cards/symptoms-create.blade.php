<card-basic title="Add Symptom" class="mt-4">
  @include('components.errors')
  <form action="{{ route('admin.symptoms.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label for="inputName">Symptom</label>
      <input type="text" class="form-control" id="inputName" placeholder="Name of symptom" name="name" required>
    </div>
    <button-submit class="float-right"/>
  </form>
</card-basic>
