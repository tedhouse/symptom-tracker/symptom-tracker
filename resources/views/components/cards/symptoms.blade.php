<card-basic title="Symptoms" href="{{ route('admin.symptoms.index') }}" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <td>Created</td>
            <td>Image</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          @foreach($symptoms as $symptom)
          <tr>
            <th scope="row">
              {{ $symptom->name }}
            </th>
            <td>
              {{ $symptom->created_at->diffForHumans() }}
            </td>
            <td>
              @if(isset($symptom->image_path))
                <img class="img-thumbnail" style="height: 32px;" src="{{ Storage::disk('public')->url($symptom->image_path) }}">
              @endif
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                <a href="{{ route('admin.symptoms.edit', ['symptom' => $symptom->id]) }}" class="dropdown-item">Edit</a>
                <a href="{{ route('admin.symptoms.image.edit', ['symptom' => $symptom->id]) }}" class="dropdown-item">Upload</a>
                <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.symptoms.destroy', ['symptom' => $symptom->id]) }}">
                  Delete
                </button>
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </template>
</card-basic>
