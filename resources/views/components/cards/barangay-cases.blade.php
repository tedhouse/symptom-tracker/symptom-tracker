<card-basic title="Cases" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Barangay</td>
            <td>Case Status</td>
            <td>Number</td>
            <td>Date</td>
            <td>Added</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          @foreach($barangay->cases as $case)
          <tr>
            <td>
              {{ $case->barangay->name }}
            </td>
            <td>
              {{ $case->caseStatus->name }}
            </td>
            <td>
              {{ $case->number }}
            </td>
            <td>
              {{ $case->date }}
            </td>
            <td>
              {{ $case->created_at ? $case->created_at->diffForHumans() : '' }}
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                <a class="dropdown-item" href="{{ route('admin.barangay-cases.edit', ['barangay_case' => $case->id]) }}">
                  Edit
                </a>
                {{-- <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.barangays.destroy', ['barangay' => $barangay->id]) }}">
                  Delete
                </button> --}}
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
