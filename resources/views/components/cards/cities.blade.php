<card-basic title="Cities" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Name</td>
            <td>Region</td>
            <td>Created</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          @foreach($cities as $city)
          <tr>
            <td>
              <a href="{{ route('admin.cities.show', ['city' => $city]) }}">
                {{ $city->name }}
              </a>
            </td>
            <td>
              {{ $city->province ? $city->province->name : '' }}
            </td>
            <td>
              {{ $city->created_at ? $city->created_at->diffForHumans() : '' }}
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                <a class="dropdown-item" href="{{ route('admin.cities.edit', ['city' => $city->id]) }}">
                  Edit
                </a>
                <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.cities.destroy', ['city' => $city->id]) }}">
                  Delete
                </button>
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
