<card-basic title="Provinces" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Name</td>
            <td>Region</td>
            <td>Created</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          @foreach($provinces as $province)
          <tr>
            <td>
              <a href="{{ route('admin.provinces.show', ['province' => $province->id]) }}">
                {{ $province->name }}
              </a>
            </td>
            <td>
              {{ $province->region ? $province->region->name : '' }}
            </td>
            <td>
              {{ $province->created_at ? $province->created_at->diffForHumans() : '' }}
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                <a class="dropdown-item" href="{{ route('admin.provinces.edit', ['province' => $province->id]) }}">
                  Edit
                </a>
                <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.provinces.destroy', ['province' => $province->id]) }}">
                  Delete
                </button>
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
