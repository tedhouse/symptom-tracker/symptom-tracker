<card-basic title="{{ $title ? $title : 'Cases' }} ({{ $date }})" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <td>Barangay</td>
          @foreach(App\CaseStatus::where('deactivated_at', null)->get() as $caseStatus)
            <td>{{ $caseStatus->name }}</td>
          @endforeach
        </thead>
        <tbody>
          @foreach($barangays as $barangay)
            <tr>
              <td>
                <a href="{{ route('admin.barangays.show', ['barangay' => $barangay->id]) }}">
                  {{ $barangay->name }}
                </a>,
                <a href="{{ route('admin.cities.show', ['city' => $barangay->city->id]) }}">
                  {{ $barangay->city->name }}
                </a>
              </td>
              @foreach(App\CaseStatus::where('deactivated_at', null)->get() as $caseStatus)
                <td>
                  {{ $barangay->cases->where('case_status_id', $caseStatus->id)->where('date', $date)->first() ? $barangay->cases->where('case_status_id', $caseStatus->id)->where('date', $date)->first()->number : '' }}
                </td>
              @endforeach
            </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
