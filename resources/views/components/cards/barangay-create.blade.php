<card-basic title="Add Barangay" class="mt-4">

  @include('components.errors')
  <form action="{{ route('admin.barangays.store') }}" method="POST">
    @csrf

    <div class="alert alert-warning" role="alert">
      If the city you want isn't showing up in the choices below, add that city first in the database. Go <a href="{{ route('admin.cities.index') }}">here</a>.
    </div>

    <div class="form-group">
      <label for="inputCity" class="m-0">City</label>
      <select class="form-control" id="inputCity" name="city" required>
        @foreach($cities as $city)
          <option value="{{ $city->id }}">{{ $city->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="inputName">Barangay Name</label>
      <input type="text" class="form-control" id="inputName" name="name" required>
    </div>

    <div class="form-group">
      <label for="inputLat">Latitude</label>
      <input type="text" class="form-control" id="inputLat" name="lat">
    </div>

    <div class="form-group">
      <label for="inputLng">Longitude</label>
      <input type="text" class="form-control" id="inputLng" name="lng">
    </div>

    <button-submit class="float-right"/>
  </form>
</card-basic>
