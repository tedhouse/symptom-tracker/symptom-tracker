<?php

use App\Barangay;
use App\Http\Resources\BarangayCollection;
use App\Http\Resources\ReportCollection;
use App\Http\Resources\ReportFullCollection;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Get all reports
Route::get('reports', function () {
    return new ReportCollection(Report::where('flagged_at', null)->where('faked_at', null)->get());
});

// Get all full reports
Route::get('reports/full', function () {
    return new ReportFullCollection(Report::all());
});

// Get all reports after a specific datetime
Route::get('reports/{datetime}', function (string $datetime) {
    return new ReportCollection(Report::where('created_at', '>=', new Carbon\Carbon($datetime))->get());
});

// Get all reports between two datetimes
Route::get('reports/{dateTimeFrom}/{dateTimeTo}', function (string $dateTimeFrom, string $dateTimeTo) {
    return new ReportCollection(
        Report::where('created_at', '>=', new Carbon\Carbon($dateTimeFrom))
            ->where('created_at', '<=', new Carbon\Carbon($dateTimeTo))
            ->get()
    );
});

// Get all barangays
Route::get('barangays', function () {
    return new BarangayCollection(Barangay::all());
});
