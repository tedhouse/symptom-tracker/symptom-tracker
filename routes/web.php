<?php

use App\Symptom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang', 'PageController@locale')->name('locale');
Route::get('lang/{locale}', 'PageController@setLocale');

// Pages
Route::get('', 'PageController@survey')->name('survey');
Route::post('', 'ReportController@store')->name('survey.store');
Route::get('thank-you', 'PageController@thanks')->name('thanks');
Route::get('faq', 'PageController@faq')->name('faq');
Route::get('map', 'PageController@map')->name('map');
Route::get('privacy', 'PageController@privacy')->name('privacy');
Route::get('about-us', 'PageController@aboutUs')->name('about-us');

// Biñan
Route::get('biñan', 'PageController@biñan')->name('biñan');
Route::get('binan', 'RedirectController@biñan');

// Auth
Auth::routes(['register' => false]);
Route::get('password/{token}', 'PasswordController@createPassword')->name('password-create');
Route::post('password/create', 'PasswordController@storePassword')->name('password-store');
// Route::get('verify/{token}', 'AdminController@verifyEmail');

Route::prefix('admin')->name('admin.')->group(function () {

    // Main admin page
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('map', 'AdminPageController@map')->name('map');

    Route::get('cases', 'AdminPageController@cases')->name('cases');

    // Resources
    Route::get('reports/problems', 'ReportController@problems')->name('reports.problems');
    Route::resource('reports', 'ReportController');
    Route::post('reports/{report}/flag', 'ReportController@flag')->name('reports.flag');
    Route::post('reports/{report}/fake', 'ReportController@fake')->name('reports.fake');
    Route::post('reports/{report}/unflag', 'ReportController@unflag')->name('reports.unflag');
    Route::post('reports/{report}/unfake', 'ReportController@unfake')->name('reports.unfake');
    Route::resource('users', 'UserController');

    Route::resource('symptoms', 'SymptomController');
    Route::get('symptoms/{symptom}/image/edit', 'SymptomController@imageEdit')->name('symptoms.image.edit');
    Route::put('symptoms/{symptom}/image/update', 'SymptomController@imageUpdate')->name('symptoms.image.update');
    Route::post('symptoms/image/none/update', 'SymptomController@noneImageUpdate')->name('symptoms.image.none.update');

    Route::resource('case-statuses', 'CaseStatusController');
    Route::put('case-statuses/{case_status}/activate', 'CaseStatusController@activate')->name('case-statuses.activate');
    Route::put('case-statuses/{case_status}/deactivate', 'CaseStatusController@deactivate')->name('case-statuses.deactivate');

    Route::resource('regions', 'RegionController');
    Route::resource('provinces', 'ProvinceController');
    Route::resource('cities', 'CityController');
    Route::resource('barangays', 'BarangayController');
    Route::resource('barangay-cases', 'BarangayCaseController');

    // Profile
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/password/update', 'ProfileController@updatePassword')->name('profile.password.update');
});

Route::get('{url}', function (string $url) {
    return redirect(route('survey'));
});
