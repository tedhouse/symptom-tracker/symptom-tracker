const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/app-md.js', 'public/js')
   .js('resources/js/vue.js', 'public/js')
   .js('resources/js/modal-functions.js', 'public/js')
   .js('resources/js/symptom-checkbox-functions.js', 'public/js')
   .js('resources/js/reports-filter.js', 'public/js')
   .js('resources/js/map.js', 'public/js')
   .js('resources/js/biñan.js', 'public/js')
   .js('resources/js/share.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/app-md.scss', 'public/css');
