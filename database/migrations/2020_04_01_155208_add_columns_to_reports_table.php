<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->boolean('travelled_abroad')->nullable()->after('sex');
            $table->boolean('close_contact')->nullable()->after('travelled_abroad');
            $table->timestamp('flagged_at')->nullable()->after('session_id');
            $table->timestamp('faked_at')->nullable()->after('flagged_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropColumn('travelled_abroad');
            $table->dropColumn('close_contact');
            $table->dropColumn('flagged_at');
            $table->dropColumn('faked_at');
        });
    }
}
