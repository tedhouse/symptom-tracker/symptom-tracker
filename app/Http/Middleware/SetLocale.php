<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('locale')) {
            \App::setLocale($request->session()->get('locale'));
        } else {
            if (in_array($request->getPreferredLanguage(), ['ceb', 'hil', 'bik', 'ilo', 'war', 'cbk', 'ko', 'ko-kr'])) {
                session(['locale' => $request->getPreferredLanguage()]);
            }
        }
        return $next($request);
    }
}
