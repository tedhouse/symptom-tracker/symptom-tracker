<?php

namespace App\Http\Controllers;

use App\Barangay;
use Illuminate\Http\Request;

class AdminPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function map()
    {
        return view('admin.map');
    }

    public function cases()
    {
        $barangays = Barangay::all();
        return view('admin.cases.index', compact('barangays'));
    }
}
