<?php

namespace App\Http\Controllers;

use App\Symptom;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function survey(Request $request)
    {
        if (!$request->session()->has('locale')) {
            return redirect(route('locale'));
        }
        $symptoms = Symptom::all();
        return view('survey', compact('symptoms'));
    }

    public function map()
    {
        return view('map');
    }

    public function biñan()
    {
        return view('cities.biñan');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function thanks()
    {
        return view('thanks');
    }

    public function faq()
    {
        return view('faq');
    }

    public function aboutUs()
    {
        return view('about-us');
    }

    public function locale()
    {
        return view('locale');
    }

    public function setLocale($string)
    {
        if (in_array($string, ['en', 'fil', 'ceb', 'hil', 'bik', 'ilo', 'war', 'cbk', 'ko', 'ko-kr'])) {
            session(['locale' => $string]);
        }
        return redirect('survey');
    }
}
