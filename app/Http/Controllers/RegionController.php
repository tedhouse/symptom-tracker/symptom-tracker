<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::all();

        return view('admin.regions.index', compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
        ]);

        $region = new Region();
        $region->name = request('name');

        if ($region->save()) {
            return redirect(route('admin.regions.index'))->with('success', 'Added new region.');
        }
        return back()->with('danger', 'Erorr adding new region.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return view('admin.regions.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        return view('admin.regions.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Region $region)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
        ]);

        $region->name = request('name');

        if ($region->save()) {
            return redirect(route('admin.regions.index'))->with('success', 'Updated region.');
        }
        return back()->with('danger', 'Erorr updating region.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        foreach ($region->provinces as $province) {

            foreach ($province->cities as $city) {

                foreach ($city->barangays as $barangays) {
                    if (!$barangays->delete()) {
                        return back()->with('danger', 'Error deleting barangay of city.');
                    }
                }

                if (!$city->delete()) {
                    return back()->with('danger', 'Error deleting city of province.');
                }
            }

            if (!$province->delete()) {
                return back()->with('danger', 'Error deleting province of region.');
            }
        }

        if ($region->delete()) {
            return back()->with('success', 'Deleted region');
        }
        return back()->with('danger', 'Error deleting region.');
    }
}
