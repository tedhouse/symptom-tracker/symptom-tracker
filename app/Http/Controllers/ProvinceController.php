<?php

namespace App\Http\Controllers;

use App\Province;
use App\Region;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();
        $regions = Region::all();

        return view('admin.provinces.index', compact('provinces', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'region' => 'integer|required|max:191',
        ]);

        $province = new Province();

        if (!Region::find(request('region'))) {
            return back()->with('warning', 'Invalid region ID.');
        }

        $province->region_id = request('region');
        $province->name = request('name');

        if ($province->save()) {
            return redirect(route('admin.provinces.index'))->with('success', 'Added new province.');
        }
        return back()->with('danger', 'Erorr adding new province.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        return view('admin.provinces.show', compact('province'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        $regions = Region::all();

        return view('admin.provinces.edit', compact('province', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'region' => 'integer|required|max:191',
        ]);

        if (!Region::find(request('region'))) {
            return back()->with('warning', 'Invalid region ID.');
        }

        $province->region_id = request('region');
        $province->name = request('name');

        if ($province->save()) {
            return redirect(route('admin.provinces.index'))->with('success', 'Updated province.');
        }
        return back()->with('danger', 'Erorr updating province.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        foreach ($province->cities as $city) {

            foreach ($city->barangays as $barangays) {
                if (!$barangays->delete()) {
                    return back()->with('danger', 'Error deleting barangay of city.');
                }
            }

            if (!$city->delete()) {
                return back()->with('danger', 'Error deleting city of province.');
            }
        }

        if ($province->delete()) {
            return back()->with('success', 'Deleted province');
        }
        return back()->with('danger', 'Error deleting province.');
    }
}
