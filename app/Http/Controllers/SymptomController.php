<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Symptom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SymptomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Only allow logged in users to access this controller
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $symptoms = Symptom::all();
        return view('admin.symptoms.index', compact('symptoms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:255',
        ]);

        $symptom = new Symptom();
        $symptom->name = request('name');

        if ($symptom->save()) {
            return redirect(route('admin.symptoms.index'));
        }
        return back()->with('danger', 'Erorr adding new symptom.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function show(Symptom $symptom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function edit(Symptom $symptom)
    {
        return view('admin.symptoms.edit', compact('symptom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Symptom $symptom)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:255',
        ]);

        $symptom->name = request('name');

        if ($symptom->save()) {
            return redirect(route('admin.symptoms.index'));
        }
        return back()->with('danger', 'Erorr updating symptom.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Symptom $symptom)
    {
        foreach ($symptom->reportSymptoms as $symptomReport) {
            if (!$symptomReport->delete()) {
                return back()->with('danger', 'Error deleting symptom.');
            }
        }

        if ($symptom->delete()) {
            return redirect(route('admin.symptoms.index'))->with('success', 'Deleted symptom');
        }
        return back()->with('danger', 'Error deleting symptom.');
    }

    public function imageEdit(Symptom $symptom)
    {
        return view('admin.symptoms.upload', compact('symptom'));
    }

    public function imageUpdate(Symptom $symptom, Request $request)
    {
        $upload = $request->image;
        if (Helper::isImage($upload)) {
            $storage_path = Storage::disk('public')->putFile('symptoms/' . $symptom->id, $upload);
            $symptom->image_path = $storage_path;
            if ($symptom->save()) {
                return back()->with('success', 'Uploaded image.');
            }
            return back()->with('warning', 'Error uploading image');
        }
        return back()->with('warning', 'Invalid file type. Please upload an image file.');
    }

    public function noneImageUpdate(Symptom $symptom, Request $request)
    {
        $upload = $request->image;
        if (Helper::isImage($upload)) {
            $result = Storage::disk('public')->putFileAs('symptoms', $upload, 'none.jpg');
            if ($result) {
                return back()->with('success', 'Uploaded image.');
            }
            return back()->with('warning', 'Error uploading image');
        }
        return back()->with('warning', 'Invalid file type. Please upload an image file.');
    }
}
