<?php

namespace App\Http\Controllers;

use DB;
use App\Report;
use App\ReportSymptom;
use App\Symptom;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('_token')) {
            $minAge = preg_replace("/[^0-9]/", "", request('min_age'));
            $maxAge = preg_replace("/[^0-9]/", "", request('max_age'));

            $minDate = request('min_date');
            $maxDate = request('max_date');

            $sex = request('sex');

            $symptomNum = request('symptom_num');

            $symptomId = request('symptom_names');

            $reportIds = Report::latest('reports.created_at')
                ->select('reports.id')
                ->leftjoin('report_symptoms', 'reports.id', '=', 'report_symptoms.report_id')
                ->when($symptomId != null && request('symptom_num_select') != 'on', function ($query) use ($symptomId) {
                    return $query
                        ->whereIn('report_symptoms.symptom_id', $symptomId);
                })
                ->when(request('sex_select') == 'on', function ($query) use ($sex) {
                    return $query
                        ->where('reports.sex', $sex);
                })
                ->when($minDate == $maxDate, function ($query) use ($minDate) {
                    return $query
                        ->whereDate('reports.created_at', '=', $minDate);
                })
                ->when($minDate != $maxDate, function ($query) use ($minDate, $maxDate) {
                    return $query
                        ->whereDate('reports.created_at', '>=', $minDate)
                        ->whereDate('reports.created_at', '<=', $maxDate);
                })
                ->when(request('age_select') == 'on', function ($query) use ($minAge, $maxAge) {
                    return $query
                        ->where('reports.age', ">=", $minAge)
                        ->where('reports.age', "<=", $maxAge);
                })
                ->when(request('symptom_num_select') == 'on', function ($query) use ($symptomNum, $symptomId) {
                    return $query
                        ->groupby('report_symptoms.report_id')
                        ->havingRaw('count(`reports`.`id`) = ' . $symptomNum);
                })
                ->get()->toArray();

            if (request('symptom_num_select') == 'on' && $symptomId != null) {
                $reports = Report::whereHas('symptoms', function ($query) use ($symptomId) {
                    $query->whereIn('report_symptoms.symptom_id', $symptomId);
                })
                    ->whereIn('reports.id', ($reportIds))->paginate(50);
            } else {
                $reports = Report::latest()
                    ->wherein('reports.id', ($reportIds))->paginate(50);
            }
        } else {
            $reports = Report::latest()->paginate(50);
        }

        $symptoms = Symptom::all();

        return view('admin.reports.index', compact('reports', 'symptoms'));
    }

    public function show(Report $report)
    {
    }

    public static function randomizeCoordinate($coordinate)
    {
        $coordinate = round($coordinate, 10);
        $lat = $coordinate;
        $decimals = explode('.', $coordinate);;
        $cut = $decimals[1];
        $random = rand(1, 25) * 0.1;
        if (rand(1, 2) == 1) {
            $random = $random * -1;
        }
        $test = substr($cut, 3) * 0.000001 * $random;
        return $lat + $test;
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'lat' => 'string|required|max:100',
            'lng' => 'string|required|max:100',
            'age' => 'integer|nullable|min:0|max:116',
            'sex' => 'integer|nullable|digits:1',
            'traveled_abroad' => ['nullable', Rule::in(['true'])],
            'close_contact' => ['nullable', Rule::in(['true'])],
        ]);

        // Only accept valid coordinates
        if (abs(request('lat')) > 90 || abs(request('lng') > 180)) {
            return back();
        }

        $report = new Report();
        $report->lat = ReportController::randomizeCoordinate(request('lat'));
        $report->lng = ReportController::randomizeCoordinate(request('lng'));
        $report->age = request('age');
        $report->sex = request('sex');
        $report->session_id = Session::getId();
        $report->ip_address = $request->ip();

        if (request('traveled_abroad') == 'true') {
            $report->traveled_abroad = 1;
        }

        if (request('close_contact') == 'true') {
            $report->close_contact = 1;
        }

        if (request('symptoms') == null && request('symptom_none') == null) {
            return redirect(route('thanks'));
        }

        if ($report->save()) {

            if (request('symptom_none') === 'true') {
                return redirect(route('thanks'));
            }

            // If there are symptoms, save them too
            foreach (request('symptoms') as $symptom) {
                if (Symptom::exists($symptom)) {
                    $reportSymptom = new ReportSymptom();
                    $reportSymptom->report_id = $report->id;
                    $reportSymptom->symptom_id = $symptom;

                    if (!$reportSymptom->save()) {
                        return back()->with('danger', 'Error reporting.');
                    }
                }
            }

            return redirect(route('thanks'));
        }
        return back()->with('danger', 'Erorr reporting.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        foreach ($report->reportSymptoms as $symptomReport) {
            if (!$symptomReport->delete()) {
                return back()->with('danger', 'Error deleting symptom.');
            }
        }

        if ($report->delete()) {
            return redirect(route('admin.reports.index'))->with('success', 'Deleted report');
        }
        return back()->with('danger', 'Error deleting report.');
    }

    // Set report as a possibly fake
    public function flag(Report $report)
    {
        $report->flagged_at = Carbon::now();

        if ($report->save()) {
            return back()->with('Flagged report.');
        }
        return back()->with('danger', 'Erorr reporting.');
    }

    // Unset report as a possibly fake
    public function unflag(Report $report)
    {
        $report->flagged_at = null;

        if ($report->save()) {
            return back()->with('Unflagged report.');
        }
        return back()->with('danger', 'Erorr reporting.');
    }

    // Set report as fake
    public function fake(Report $report)
    {
        $report->faked_at = Carbon::now();

        if ($report->save()) {
            return back()->with('Report confirmed as fake.');
        }
        return back()->with('danger', 'Erorr reporting.');
    }

    // Unset report as fake
    public function unfake(Report $report)
    {
        $report->faked_at = null;

        if ($report->save()) {
            return back()->with('Report declared not fake.');
        }
        return back()->with('danger', 'Erorr reporting.');
    }

    // All flagged or faked reports
    public function problems()
    {
        $reports = Report::where('flagged_at', '!=', null)->orWhere('faked_at', '!=', null)->latest()->paginate(50);

        return view('admin.reports.problems', compact('reports'));
    }
}
