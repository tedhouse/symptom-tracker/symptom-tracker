<?php

namespace App\Http\Controllers;

use Log;
use Hash;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updatePassword(Request $request)
    { 
        $validatedData = $request->validate([
            'current' => 'required|string|max:255',
            'new' => 'required|string|max:255',
            'confirm' => 'required|string|max:255',
        ]);

        $admin = Auth::user();
        if(Hash::check($request->current, $admin->password)) {
            if($request->new == $request->confirm) {
                $admin->password = bcrypt($request->new);
            if($admin->save()) {
                return redirect(route('admin.profile'))->with('success', 'Updated password.');
            }
            else {
                return back()->with('danger', 'Error in updating password.');
            }
          }
          return back()->with('danger', 'Passwords do not match.');
        }
        return back()->with('danger', 'Incorrect password.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
