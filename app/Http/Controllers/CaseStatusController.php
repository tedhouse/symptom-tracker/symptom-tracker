<?php

namespace App\Http\Controllers;

use App\CaseStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CaseStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $caseStatuses = CaseStatus::all();

        return view('admin.case-statuses.index', compact('caseStatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:255',
        ]);

        $caseStatus = new CaseStatus();
        $caseStatus->name = request('name');

        if ($caseStatus->save()) {
            return redirect(route('admin.case-statuses.index'))->with('success', 'Added new case status.');
        }
        return back()->with('danger', 'Erorr adding new case status.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CaseStatus  $caseStatus
     * @return \Illuminate\Http\Response
     */
    public function show(CaseStatus $caseStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CaseStatus  $caseStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseStatus $caseStatus)
    {
        return view('admin.case-statuses.edit', compact('caseStatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CaseStatus  $caseStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CaseStatus $caseStatus)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:255',
        ]);

        $caseStatus->name = request('name');

        if ($caseStatus->save()) {
            return redirect(route('admin.case-statuses.index'))->with('success', 'Updated case status.');
        }
        return back()->with('danger', 'Erorr updating case status.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CaseStatus  $caseStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaseStatus $caseStatus)
    {
        if ($caseStatus->delete()) {
            return back()->with('success', 'Deleted case status.');
        }
        return back()->with('danger', 'Error deleting case status.');
    }

    public function activate(CaseStatus $caseStatus)
    {
        $caseStatus->deactivated_at = null;

        if ($caseStatus->save()) {
            return back()->with('success', 'Activated case status.');
        }
        return back()->with('danger', 'Erorr activating case status.');
    }

    public function deactivate(CaseStatus $caseStatus)
    {
        $caseStatus->deactivated_at = Carbon::now();

        if ($caseStatus->save()) {
            return back()->with('success', 'Deactivated case status.');
        }
        return back()->with('danger', 'Erorr deactivating case status.');
    }
}
