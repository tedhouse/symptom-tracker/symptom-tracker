<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\NewUser;
use Log;
use Hash;
use App\User;
use App\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:users',
        ]);

        $admin = new User();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make(Helper::generateRandomString(20));

        if ($admin->save()) {
            Mail::to($admin->email)->send(new NewUser($admin));
            return redirect(route('admin.users.index'))->with('success', 'Sent verification email to admin.');
        }
        return back()->with('danger', 'Error adding new admin.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $logins = Login::latest()
                    ->where('user_id', $user->id)
                    ->paginate(50);
        
        return  view('admin.users.show', compact('logins', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete() && strcmp($user->email, "cyrus_vatandoostkakhki@dlsu.edu.ph") != 0) {
            return redirect(route('admin.users.index'))->with('success', 'Deleted user');
        }
        return back()->with('danger', 'Error deleting user.');
    }
}
