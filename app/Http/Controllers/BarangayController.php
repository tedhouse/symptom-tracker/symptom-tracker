<?php

namespace App\Http\Controllers;

use App\Barangay;
use App\CaseStatus;
use App\City;
use Illuminate\Http\Request;

class BarangayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangays = Barangay::all();
        $cities = City::all();
        $caseStatuses = CaseStatus::all();

        return view('admin.barangays.index', compact('barangays', 'cities', 'caseStatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'city' => 'integer|required|max:191',
            'lat' => 'string|required|max:100',
            'lng' => 'string|required|max:100',
        ]);

        // Only accept valid coordinates
        if (abs(request('lat')) > 90 || abs(request('lng') > 180)) {
            return back();
        }

        $barangay = new Barangay();

        if (!City::find(request('city'))) {
            return back()->with('warning', 'Invalid city ID.');
        }

        $barangay->city_id = request('city');
        $barangay->name = request('name');
        $barangay->lat = request('lat');
        $barangay->lng = request('lng');

        if ($barangay->save()) {
            return back()->with('success', 'Added new barangay.');
        }
        return back()->with('danger', 'Erorr adding new barangay.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barangay  $barangay
     * @return \Illuminate\Http\Response
     */
    public function show(Barangay $barangay)
    {
        $caseStatuses = CaseStatus::all();

        return view('admin.barangays.show', compact('barangay', 'caseStatuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barangay  $barangay
     * @return \Illuminate\Http\Response
     */
    public function edit(Barangay $barangay)
    {
        $cities = City::all();

        return view('admin.barangays.edit', compact('barangay', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barangay  $barangay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barangay $barangay)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'city' => 'integer|required|max:191',
            'lat' => 'string|required|max:100',
            'lng' => 'string|required|max:100',
        ]);

        // Only accept valid coordinates
        if (abs(request('lat')) > 90 || abs(request('lng') > 180)) {
            return back();
        }

        if (!City::find(request('city'))) {
            return back()->with('warning', 'Invalid city ID.');
        }

        $barangay->city_id = request('city');
        $barangay->name = request('name');
        $barangay->lat = request('lat');
        $barangay->lng = request('lng');

        if ($barangay->save()) {
            return back()->with('success', 'Updated barangay.');
        }
        return back()->with('danger', 'Erorr updating barangay.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barangay  $barangay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barangay $barangay)
    {
        if ($barangay->delete()) {
            return back()->with('success', 'Deleted barangay');
        }
        return back()->with('danger', 'Error deleting barangay.');
    }
}
