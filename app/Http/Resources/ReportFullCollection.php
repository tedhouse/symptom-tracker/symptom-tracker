<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportFullCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($report) {
                return [
                    'id' => $report->id,
                    'lat' => $report->lat,
                    'lng' => $report->lng,
                    'symptoms' => new SymptomCollection($report->symptoms),
                    'age' => $report->age,
                    'sex' => $report->sex,
                    'traveled_abroad' => $report->traveled_abroad,
                    'close_contact' => $report->close_contact,
                    'flagged_at' => $report->flagged_at,
                    'faked_at' => $report->faked_at,
                    'created_at' => $report->created_at,
                ];
            }),
        ];
    }
}