<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BarangayCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($barangay) {
                return [
                    'id' => $barangay->id,
                    'name' => $barangay->name,
                    'lat' => $barangay->lat,
                    'lng' => $barangay->lng,
                ];
            }),
        ];
    }
}
