<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Symptom extends Model
{
    use SoftDeletes;

    public function reportSymptoms()
    {
        return $this->hasMany('App\ReportSymptom');
    }
}
