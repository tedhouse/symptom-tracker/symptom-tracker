<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barangay extends Model
{
    use SoftDeletes;

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function cases()
    {
        return $this->hasMany('App\BarangayCase');
    }
}
