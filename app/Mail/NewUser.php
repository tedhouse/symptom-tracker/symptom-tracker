<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->token = \Password::getRepository()->create($user);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@imagineware.ph', 'SymptomTracker')
            ->subject('New Admin Verification')
            ->markdown('mails.verification-password')
            ->with([
                'name' => $this->user->name,
                'token' => $this->token,
                'link' => config('app.url') . '/password/' . $this->token . '?email=' . $this->user->email,
            ]);
    }
}
