<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CovidCase extends Model
{
    use SoftDeletes;

    public function status()
    {
        return $this->belongsTo('App\CaseStatus', 'case_status_id', 'id');
    }

    public function addedBy()
    {
        return $this->belongsTo('App\User', 'added_by', 'id');
    }
}
