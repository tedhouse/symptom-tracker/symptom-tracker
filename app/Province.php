<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function cities()
    {
        return $this->hasMany('App\City');
    }
}
