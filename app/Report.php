<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'flagged_at' => 'datetime',
        'faked_at' => 'datetime',
    ];

    public function reportSymptoms()
    {
        return $this->hasMany('App\ReportSymptom');
    }

    public function symptoms()
    {
        return $this->hasManyThrough('App\Symptom', 'App\ReportSymptom', 'report_id', 'id', 'id', 'symptom_id');
    }
}
